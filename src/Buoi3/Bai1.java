package Buoi3;

import java.util.Scanner;

//nhap 2 so nguyen a,b.in ra menu va su dung ham de thuc hien chucs nang sau
//1.tinh tong
//2.tinh hieu
//3.tinh tich
//4.tinh thuong
public class Bai1 {
    public static void main(String[] asgs){
        int chon;
        Scanner sc=new Scanner(System.in);
        int a;
        int b;
        System.out.println("nhap 2 so a,b: ");
        a=sc.nextInt();
        b=sc.nextInt();
        System.out.println("moi ban chon chuc nang: ");
        System.out.println("1.Tinh tong");
        System.out.println("2.Tinh hieu");
        System.out.println("3.Tinh tich");
        System.out.println("4.Tinh thuong");
        do{
            chon=sc.nextInt();
            switch (chon){
                case 1:
                {
                    Sum(a,b);
                    break;
                }
                case 2:
                {
                    Hieu(a,b);
                    break;
                }
                case 3:
                {
                    Tich(a,b);
                    break;
                }
                case 4:
                {
                    Thuong(a,b);
                    break;
                }
            }
        }
        while (chon!=5);
    }
    private static void Sum(int a, int b){
        int c=a+b;
        System.out.println("Tong a+b= "+c);
    }
    private static void Hieu(int a, int b){
        int c=a-b;
        System.out.println("Hieu a-b= "+c);
    }
    private static void Tich(int a, int b){
        int c=a*b;
        System.out.println("Tich a*b= "+c);
    }
    private static void Thuong(int a, int b){
        int c=a/b;
        System.out.println("Thuong a/b= "+c);
    }
}
