package Buoi3.btvn;

import java.util.Scanner;

public class bai1 {
    public static void main(String[] args){
        int n;
        int min = 0;
        int max =0;
        Scanner sc=new Scanner(System.in);
        int arr[] = new int[100];
        System.out.println("Nhap so luong phan tu trong mang n= ");
        n=sc.nextInt();
        Nhap(arr, n);
       min = Min(arr,n);
       max= Max(arr, n);
        System.out.println("Min: "+min);
        System.out.println("Max: "+max);
        SonguyenChan(arr,n);
        Uoc(arr, n,max);
        Boiso(arr, n,min);
    }
    private static void Nhap(int a[], int n){
        for (int i=0; i<n;i++){
            Scanner sc=new Scanner(System.in);
            System.out.print("phan thu thu "+i+" : ");
            a[i]=sc.nextInt();
        }
        System.out.println("\n");
    }
    private static int Min(int a[], int n){
        int min=a[0];
        for(int i=0;i<n;i++){
            if(a[i]<min){
                min=a[i];
            }
        }
        return min;
    }
    private static int Max(int a[], int n){
        int max=a[0];
        for(int i=0;i<n;i++){
            if(a[i]>max){
                max=a[i];
            }
        }
        return max;
    }
    private static void SonguyenChan(int a[], int n){
        System.out.println("danh sach cac so nguyen chia het cho 2 va 3 la: ");
        for(int i=0; i<n;i++){
            if(a[i]%2==0 &&a[i]%3==0){
                System.out.print(a[i]+"\t");
            }
        }
        System.out.println("\n");
    }
    private static void Uoc(int a[], int n,int max){
        System.out.println("Cac uoc cua Max: ");
        for(int i=0; i<n;i++){
            if(max%a[i]==0){
                System.out.print(a[i]+"\t");
            }
        }
        System.out.println("\n");
    }
    private static void Boiso(int a[], int n,int min){
        System.out.println("cac so la boi so cua Min: ");
        for(int i=0;i<n;i++){
            if(a[i]%min==0){
                System.out.print(a[i]+"\t");
            }
        }
        System.out.println("\n");
    }
}
