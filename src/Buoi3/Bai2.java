package Buoi3;

import java.util.Scanner;

//nhap mang n phan tu. su dung ham de thuc hien cac chuc nang sau
//1.hiem thi day so tang dan
//2.nhap tu ban phim 1 so bat ki, kiem tra so do co nam trong mang hay khong
//3.hiem thi ra man hinh so nguyen to trong mang
public class Bai2 {
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        int n;
        System.out.println("nhap vao so phan tu cua mang: ");
        n=sc.nextInt();
        int arr[]=new int[100];
        Nhap(arr,n);
        //Sapxep(arr, n);
        //kiemTra(arr,n);
        for (int i = 0; i < n; i++) {
            if (SNT(arr[i]) == 1) System.out.println(arr[i] + " la so nguyen to");
        }
    }
    private static void Nhap(int arr[], int n){
        Scanner sc=new Scanner(System.in);
        for (int i=0; i<n;i++){
            arr[i]=sc.nextInt();
        }
        System.out.println("Danh sach cac phan tu: ");
        for (int i=0; i<n;i++){
            System.out.print(arr[i]+ "\t");
        }
        System.out.println("");
    }
    private static void Sapxep(int arr[], int n){
        int tmp;
        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                if(arr[i]>arr[j])
                {
                    tmp=arr[i];
                    arr[i]=arr[j];
                    arr[j]=tmp;
                }
            }
        }
        System.out.println("Danh sách các phần tử theo thứ tự tăng dần là:\t");

        for (int i = 0; i <n; i++) {

            System.out.print(arr[i]+"\t");
        }
        System.out.println("");
    }
    public static void kiemTra(int arr[], int n){
        Scanner sc=new Scanner(System.in);
        System.out.println("nhap so can kiem tra: ");
        int k=sc.nextInt();
        int tmp=0;
        for (int i=0;i<n; i++){
            if(arr[i]==k){
                tmp=0;
                System.out.println(k+" co trong mang");
                break;
            }
            else tmp=1;
        }
        if (tmp==1){
            System.out.println(k+" khong co trong mang");
        }
    }
    private static int SNT( int x){
        int dem=0;
        for(int i=1; i<=x; i++) {
            if (x % i == 0) {
                dem++;
            }
        }
        if (dem==2)
        {
            return 1;
        }
        else{
            return 0;
        }
    }

}
