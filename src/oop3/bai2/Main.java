package oop3.bai2;

public class Main {
    public static void main(String[] args){
        Vang vang1 = new Vang(1,23,4,20009,67,4);
        vang1.setLoai(3);
        Vang vang2 = new Vang(2,24,5,2008,45,2);
        vang2.setLoai(2);
        TienTe tien1 = new TienTe(1,23,6,2000,345,2);
        tien1.setLoai("usd");
        tien1.setTiGia(2);
        TienTe tien2 = new TienTe(2,3,4,2008,124,4);
        tien2.setLoai("Vn");
        tien2.setTiGia(4);
        System.out.println("Thông tin");
        vang1.Xuat();
        vang2.Xuat();
        vang1.Tong(vang1, vang2);
        tien1.Xuat();
        tien2.Xuat();
        tien1.Tong(tien1,tien2);
    }
}
