package oop3.bai3;

public class Nha extends giaoDich {
    private String LoaiNha;
    private String DiaChi;

//    public Nha(int MaGiaoDich, int ngay, int thang, int nam, int donGia, int DienTich){
//        super(MaGiaoDich,ngay,thang,nam,donGia, DienTich);
//    }
    public Nha(){
        super();
    }
    public Nha(String LoaiNha, String DiaChi) {
        super();
        this.LoaiNha =  LoaiNha;
        this.DiaChi = DiaChi;
    }

    public String getLoaiNha() {
        return LoaiNha;
    }

    public void setLoaiNha(String loaiNha) {
        LoaiNha = loaiNha;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public void setDiaChi(String diaChi) {
        DiaChi = diaChi;
    }

    void Nhap(){
        super.Nhap();
        sc.nextLine();
        System.out.println("Loại Nhà: ");
        setLoaiNha(sc.nextLine());

        System.out.println("Địa chỉ: ");
        setDiaChi(sc.nextLine());
    }
    void Xuat(){
        super.Xuat();
        System.out.println("Loại nhà\t Địa chỉ\t");
        System.out.println(this.LoaiNha+"\t\t"+ this.DiaChi+"\t\t");
    }
}
