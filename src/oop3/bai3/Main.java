package oop3.bai3;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void  main(String[] args) {
        ArrayList<Nha> arrNha = new ArrayList<>();
        ArrayList<Dat> arrDat = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        int dem;
        System.out.print("Nhập số giao dịch đất, nhà: ");
        dem = scanner.nextInt();
        System.out.println("Nhập thông tin giao dịch nhà:");
        for (int i = 0; i < dem; i++) {
            System.out.println("Nhập thông tin giao dịch nhà thứ " + (i + 1) + ":");
            Nha nha1 = new Nha();
            nha1.Nhap();
            arrNha.add(nha1);
        }
        System.out.println("Nhập thông tin giao dịch đất:");
        for (int i = 0; i < dem; i++) {
            System.out.println("Nhập thông tin giao dịch đất thứ " + (i + 1) + ":");
            Dat dat1 = new Dat();
            dat1.Nhap();
            arrDat.add(dat1);
        }
        System.out.println("---Thông tin các giao dịch đất---");
        for (int i = 0; i < arrDat.size(); i++) {
            arrDat.get(i).Xuat();
        }

        System.out.println("---Thông tin các giao dịch nhà---");
        for (int i = 0; i < arrNha.size(); i++) {
            arrNha.get(i).Xuat();
        }

        int Tong = 0;
        for(int i = 0; i < arrDat.size(); i++) {

            if (arrDat.get(i).getLoaiDat().equalsIgnoreCase("A")) {
                Tong += arrDat.get(i).getDienTich() * arrDat.get(i).getdonGia() * 1.5;
            } else if (arrDat.get(i).getLoaiDat().equalsIgnoreCase("B") || arrDat.get(i).getLoaiDat().equalsIgnoreCase("C")) {
                Tong += arrDat.get(i).getDienTich() * arrDat.get(i).getdonGia();
            }
        }
        float TB = Tong / arrDat.size();
        System.out.println("Trung bình thành tiền của giao dịch đất = " + TB);
        System.out.println("Các giao dịch đất của tháng 2 năm 2020: ");
        for (int i = 0; i < arrDat.size(); i++) {
            if(arrDat.get(i).getThang() == 2 && arrDat.get(i).getNam() ==2020){
                arrDat.get(i).Xuat();
            }
        }

        System.out.println("Các giao dịch nhà của tháng 2 năm 2020: ");
        for (int i = 0; i < arrNha.size(); i++) {
            if(arrNha.get(i).getThang() == 2 && arrNha.get(i).getNam() ==2020){
                arrNha.get(i).Xuat();
            }
        }
    }
}
