package oop4.bai1;

import java.util.Scanner;

public class HoaDon {
    int MaHoaDon;
    int Ngay;
    int Thang;
    int Nam;
    String Ten;
    int MaPhong;
    int DonGia;
    Scanner sc = new Scanner(System.in);
    public HoaDon(int MaHoaDon, int Ngay, int Thang, int Nam, int MaPhong, int DonGia, String Ten){
        this.MaHoaDon = MaHoaDon;
        this.Ngay = Ngay;
        this.Thang = Thang;
        this.Nam =Nam;
        this.MaPhong = MaPhong;
        this.DonGia = DonGia;
        this.Ten = Ten;
    }
    public HoaDon(){
        super();
    }

    void Nhap(){
        System.out.println("Mã hóa đơn: ");
        MaHoaDon = sc.nextInt();
        System.out.println("Ngày tháng năm: ");
        Ngay = sc.nextInt();
        Thang = sc.nextInt();
        Nam = sc.nextInt();
        System.out.println("Mã phòng: ");
        MaPhong = sc.nextInt();
        System.out.println("Đơn giá: ");
        DonGia = sc.nextInt();
        sc.nextLine();
        System.out.println("Tên khách hàng: ");
        Ten = sc.nextLine();
    }

    void Xuat(){
        System.out.println("Mã hóa đơn \t Ngày \t Mã Phòng\t Đơn giá\t Tên khách hàng");
        System.out.println(this.MaHoaDon+"\t\t"+ this.Ngay+"/"+this.Thang+"/"+this.Nam+"\t\t"+ this.MaPhong+"\t\t"+ this.DonGia+"\t\t"+ this.Ten);
    }
    int Thanhtien(){
        return 0;
    }
}

