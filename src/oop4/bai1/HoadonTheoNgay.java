package oop4.bai1;

import javafx.scene.transform.Scale;

import java.util.Scanner;

public class HoadonTheoNgay extends HoaDon {
    private int Songay;
    private int Tien ;
    public HoadonTheoNgay(int MaHoaDon, int Ngay, int Thang, int Nam, int MaPhong, int DonGia, String Ten){
        super(MaHoaDon,Ngay,Thang,Nam,MaPhong,DonGia,Ten);
    }
    public HoadonTheoNgay(){
        super();
    }
    Scanner sc = new Scanner(System.in);
    public int getSongay() {
        return Songay;
    }

    public void setSongay(int songay) {
        Songay = songay;
    }

    public int getTien() {
        return Tien;
    }

    public void setTien(int tien) {
        Tien = tien;
    }
    @Override
    void Nhap(){
        super.Nhap();
        System.out.println("số ngày; ");
        setSongay(sc.nextInt());
    }
    void Xuat(){
        super.Xuat();
        System.out.println("Số ngày: "+ this.Songay);
        float tien =0;
        if(this.Songay <= 7){
            tien = this.DonGia * this.Songay;
        }else{
            tien = (float)((this.DonGia * 7) + (this.DonGia * (this.Songay-7))*0.8);
        }
        System.out.println("Thành tiền: "+ tien);
    }
     int Thanhtien(){
        float tien =0;
        if(this.Songay <= 7){
            tien = this.DonGia * this.Songay;
        }else{
            tien = (float)((this.DonGia * 7) + (this.DonGia * (this.Songay-7))*0.8);
        }
        return (int)tien;
    }
}
