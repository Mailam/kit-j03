package oop4.bai1;

import java.util.Scanner;
import java.util.StringJoiner;

public class HoadonTheoGio extends HoaDon {
    private int Gio;
    private int Tien;
    public HoadonTheoGio(int MaHoaDon, int Ngay, int Thang, int Nam, int MaPhong, int DonGia, String Ten){
        super(MaHoaDon,Ngay,Thang,Nam,MaPhong,DonGia,Ten);
    }
    public HoadonTheoGio(){
        super();
    }
    Scanner sc = new Scanner(System.in);
    public int getGio() {
        return Gio;
    }

    public void setGio(int gio) {
        Gio = gio;
    }

    public int getTien() {
        return Tien;
    }

    public void setTien(int tien) {
        Tien = tien;
    }
    @Override
    void Nhap(){
        super.Nhap();
        System.out.println(" Số giờ: ");
        setGio(sc.nextInt());
    }
    @Override
    void Xuat(){
        super.Xuat();
        System.out.println("Số giờ: "+ this.Gio);
        int tien = 0;
        if(this.Gio < 24){
            tien = this.DonGia * this.Gio;
        }
        else if(this.Gio > 24 && this.Gio <30){
            tien = this.DonGia *24;
        }else{
            System.out.println("...");
        }
        System.out.println("Thành tiền"+ tien);
    }
    int Thanhtien(){
        super.Thanhtien();
        int tien = 0;
        if(this.Gio < 24){
            tien = this.DonGia * this.Gio;
        }
        else if(this.Gio > 24 && this.Gio <30){
            tien = this.DonGia *24;
        }else{
            System.out.println("...");
        }
        return tien;
    }
}
