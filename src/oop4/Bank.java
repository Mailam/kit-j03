package oop4;

public class Bank {
    private String name;

    /// OverLoading
    //  Đây là khả năng cho phép một lớp có nhiều thuộc tính,
    //  phương thức cùng tên nhưng với các tham số khác nhau
    //  về loại cũng như về số lượng. Khi được gọi, dựa vào
    //  tham số truyền vào, phương thức tương ứng sẽ được thực hiện.
    public Bank(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // Override
    // là hai phương thức cùng tên, cùng tham số,
    // cùng kiểu trả về nhưng thằng con viết lại
    // và dùng theo cách của nó, và xuất hiện ở
    // lớp cha và tiếp tục xuất hiện ở lớp con.
    // Khi dùng override, lúc thực thi, nếu lớp
    // Con không có phương thức riêng, phương thức
    // của lớp Cha sẽ được gọi, ngược lại nếu có,
    // phương thức của lớp Con được gọi.
    public String getRateOfInterest(int index, String text) {


        return "Begin";
    }
}