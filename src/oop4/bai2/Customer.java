package oop4.bai2;

import java.util.Scanner;

public class Customer extends Person {
    private String CongTi;
    private int HoaDon;
    private String danhGia;
    Scanner sc =  new Scanner(System.in);
    public String getCongTi() {
        return CongTi;
    }

    public void setCongTi(String congTi) {
        CongTi = congTi;
    }

    public int getHoaDon() {
        return HoaDon;
    }

    public void setHoaDon(int hoaDon) {
        HoaDon = hoaDon;
    }

    public String getDanhGia() {
        return danhGia;
    }

    public void setDanhGia(String danhGia) {
        this.danhGia = danhGia;
    }
    public void NhapThongTin(){
        super.NhapThongTin();
        System.out.println("Tên công ty: ");
        setCongTi(sc.nextLine());
        System.out.println("Đánh giá: ");
        setDanhGia(sc.nextLine());
        System.out.println("Trị giá hóa đơn: ");
        setHoaDon(sc.nextInt());

    }
    @Override
    public String toString() {
        return super.toString() +", Tên Công Ty: "+ this.CongTi + ", Hóa Đơn: "+this.HoaDon+", Đánh giá: "+ this.danhGia;
    }
}
