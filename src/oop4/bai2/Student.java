package oop4.bai2;

import java.util.Scanner;

public class Student extends Person {
    private int Subjects1;
    private int Subjects2;
    Scanner sc =  new Scanner(System.in);
    public Student(String Name, String Add){
        super(Name, Add);
    }
    public Student(){
        super();
    }
    public int getSubjects1() {
        return Subjects1;
    }

    public void setSubjects1(int subjects1) {
        Subjects1 = subjects1;
    }

    public int getSubjects2() {
        return Subjects2;
    }

    public void setSubjects2(int subjects2) {
        Subjects2 = subjects2;
    }

    int Medium(){
        return (Subjects1 + Subjects2)/ 2;
    }
    String Evaluate(){
        if(Medium() >=8 ){
            return "Tốt" ;
        }
        else if(Medium() >=5 && Medium() <= 7){
            return "Khá";
        }
        else {
           return "Kém";
        }
    }
    public void NhapThongTin(){
        super.NhapThongTin();
        System.out.println("Điểm Môn Học 1: ");
        setSubjects1(sc.nextInt());
        System.out.println("Điểm Môn Học 2: ");
        setSubjects2(sc.nextInt());
    }
    @Override
    public String toString() {
        return super.toString() + ", Điểm môn 1: "+ Subjects1 +", Điểm môn 2: "+ Subjects2 +", Trung bình môn: "+ Medium()+
                ", Đánh giá: "+ Evaluate();
    }
}
