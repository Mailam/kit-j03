package oop4.bai2;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Person person;
        int dem;
        int chon;
        ArrayList<Person> arrPerson = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap thong tin");
        System.out.println("1.Sinh vien");
        System.out.println("2.Nhan vien");
        System.out.println("3.khach hang");
        System.out.println("4.dung chuong trinh");
        do {
            System.out.println("Chon chuc nang: ");
            chon = sc.nextInt();
            switch (chon) {
                case 1: {
                    System.out.println("Nhap so sinh vien: ");
                    dem = sc.nextInt();
                    for (int i = 0; i < dem; i++) {
                        person = new Student();
                        person.NhapThongTin();
                        arrPerson.add(person);
                    }
                    System.out.println("====THONG TIN=======");
                    for (var i = 0; i < arrPerson.size(); i++) {
                        System.out.println(arrPerson.get(i).toString());
                    }
                    break;
                }
                case 2: {
                    System.out.println("Nhap so nhan vien");
                    dem = sc.nextInt();
                    for (int i = 0; i < dem; i++) {
                        person = new Employee();
                        person.NhapThongTin();
                        arrPerson.add(person);
                    }
                    System.out.println("====THONG TIN=======");
                    for (int i = 0; i < arrPerson.size(); i++) {
                        System.out.println(arrPerson.get(i).toString());
                    }
                    break;
                }
                case 3: {
                    System.out.println("Nhap so khach hang: ");
                    dem = sc.nextInt();
                    for (int i = 0; i < dem; i++) {
                        person = new Customer();
                        person.NhapThongTin();
                        arrPerson.add(person);
                    }
                    System.out.println("====THONG TIN=======");
                    for (var i = 0; i < arrPerson.size(); i++) {
                        System.out.println(arrPerson.get(i).toString());
                    }
                    break;
                }
                default:
                    System.out.println("khong co chuc nang");
            }
        }while (chon !=4);

    }
}


