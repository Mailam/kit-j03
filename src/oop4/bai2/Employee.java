package oop4.bai2;

import oop3.bai3.Dat;

import java.util.Scanner;

public class Employee extends Person{
    private int HesoLuong;
    Scanner sc =  new Scanner(System.in);
    public Employee(String Name, String Add){
        super(Name,Add);
    }
    public Employee(){
        super();
    }
    public int getHesoLuong() {
        return HesoLuong;
    }

    public void setHesoLuong(int hesoLuong) {
        HesoLuong = hesoLuong;
    }
    int TinhLuong(){
        return this.HesoLuong * 2000000;
    }
    String DanhGia(){
        if(TinhLuong() <= 2000000){
            return "B";
        }
        else {
            return  "A";
        }
    }
    public void NhapThongTin(){
        super.NhapThongTin();
        System.out.println("Hệ số lương: ");
        setHesoLuong(sc.nextInt());
    }
    @Override
    public String toString() {
        return super.toString() +", Hệ số Lương: "+HesoLuong+ ", Lương: "+ TinhLuong() +", Đánh giá: "+ DanhGia();
    }
}
