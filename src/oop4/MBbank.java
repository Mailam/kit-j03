package oop4;

public class MBbank extends Bank {

    public MBbank(String name) {
        super(name);
    }

    @Override
    public String getRateOfInterest(int index, String text) {
        String firstText = "MB";
        String secondText = "Bank";
        return firstText + secondText;
    }
}