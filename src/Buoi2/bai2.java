package Buoi2;

import java.util.Scanner;

public class bai2 {
    public static void main(String[] args)
    {
        Scanner sc=new Scanner(System.in);
        int n;
        int[] arr=new int[100];
        int i;
        System.out.println("Nhập số phần tử của mảng: ");
        n=sc.nextInt();
        for (i = 0; i < n; i++) {
            System.out.print("Nhập vào phần tử thứ " + i + ": ");
            arr[i] = sc.nextInt();
        }
        System.out.print("Cac phan tu của mang la:\t");
        for (i = 0; i < n; i++) {
            System.out.print(arr[i]+"\t");
        }
        System.out.println("");
        int tong=0;
        for (i = 0; i < n; i++) {
            tong=tong+arr[i];
        }
        System.out.println("Tổng các phần tử của mảng là: "+ tong);
    }
}
