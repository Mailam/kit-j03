package tinhdahinh.bai1;

public class invoice {
        // Thuoc tinh - Methor
        private String id;
        private String date;
        private String customer;
        private String roomId;
        private int price;

        public invoice(String id, String date, String customer, String roomId, int price) {
            this.id = id;
            this.date = date;
            this.customer = customer;
            this.roomId = roomId;
            this.price = price;
        }

        public invoice(String id, String date, String customer, String roomId) {
            this.id = id;
            this.date = date;
            this.customer = customer;
            this.roomId = roomId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getCustomer() {
            return customer;
        }

        public void setCustomer(String customer) {
            this.customer = customer;
        }

        public String getRoomId() {
            return roomId;
        }

        public void setRoomId(String roomId) {
            this.roomId = roomId;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public double subPrice() {
            return 0;
        }
}

