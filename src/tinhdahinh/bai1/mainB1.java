package tinhdahinh.bai1;

public class mainB1 {
    public static void main(String[] args) {
        InvoiceHour invoiceHour1 = new InvoiceHour(2.3, "1", "20/09/2019", "Hoang Anh Tuan", "0034", 799);
        InvoiceHour invoiceHour2 = new InvoiceHour(5.4, "2", "23/11/2019", "Fuy Nguyen", "0057", 879);
        InvoiceDay invoiceDay1 = new InvoiceDay(1.2, "3", "12/09/2019", "Lam Doan", "0057", 599);
        InvoiceDay invoiceDay2 = new InvoiceDay(4, "4", "22/4/2020", "Duy Cung", "0057", 549);
        double totalPrice = 0;
        int temp = 0;
        invoice[] invoiceArrays = {invoiceDay1, invoiceDay2, invoiceHour1, invoiceHour2};
        for (int i = 0; i < invoiceArrays.length; i++) {
            invoice invoice = invoiceArrays[i];
            if (invoice.getDate().contains("/09/2019")) {
                totalPrice = totalPrice + invoice.subPrice();
                temp++;
            }
        }
        System.out.println("trung bình thành tiền của hóa đơn thuê phòng trong tháng 9/2019:");
        System.out.println((totalPrice) / temp);
    }
}
