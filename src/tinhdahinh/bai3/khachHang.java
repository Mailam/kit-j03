package tinhdahinh.bai3;

public class khachHang {
    String Ten;
    int maKhachHang;
    String Ngay;
    int soLuong;
    int donGia;

    public khachHang(String Ten, int maKhachHang, String Ngay, int soLuong, int donGia) {
        this.Ten = Ten;
        this.maKhachHang = maKhachHang;
        this.Ngay = Ngay;
        this.soLuong = soLuong;
        this.donGia = donGia;
    }

    public String getTen() {
        return Ten;
    }

    public void setTen(String ten) {
        Ten = ten;
    }

    public int getMaKhachHang() {
        return maKhachHang;
    }

    public void setMaKhachHang(int maKhachHang) {
        this.maKhachHang = maKhachHang;
    }

    public String getNgay() {
        return Ngay;
    }

    public void setNgay(String ngay) {
        Ngay = ngay;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    @Override
    public String toString() {
        return "Ten: "+ this.Ten+"  Ma khach Hang: "+ this.maKhachHang +"  Ngay: "+ this.Ngay+"  so luong: "+ this.soLuong+"  don gia: "+ this.donGia ;
    }
}
