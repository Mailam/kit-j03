package tinhdahinh.bai3;

public class khachHangNuocNgoai  extends  khachHang{
    private String quocTich;
    private int Thanhtien;
    public khachHangNuocNgoai(String Ten, int maKhachHang, String Ngay, int soLuong, int donGia, String quocTich) {
        super(Ten, maKhachHang, Ngay, soLuong, donGia);
        this.quocTich = quocTich;
    }

    public String getQuocTich() {
        return quocTich;
    }

    public void setQuocTich(String quocTich) {
        this.quocTich = quocTich;
    }
    public int Thanhtien(){
        Thanhtien = soLuong*donGia;
        return Thanhtien;
    }

    @Override
    public String toString() {
        return super.toString()+"  quốc tịch: "+ this.quocTich+"  Thành tiền: "+ this.Thanhtien();
    }
}
