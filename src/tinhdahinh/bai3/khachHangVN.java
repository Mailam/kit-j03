package tinhdahinh.bai3;

public class khachHangVN extends khachHang {
    private String doiTuong;
    private int dinhMuc;
    private int Thanhtien;
    public khachHangVN(String Ten, int maKhachHang, String Ngay, int soLuong, int donGia, String doiTuong, int dinhMuc) {
        super(Ten, maKhachHang, Ngay, soLuong, donGia);
        this.doiTuong = doiTuong;
        this.dinhMuc = dinhMuc;
    }

    public String getDoiTuong() {
        return doiTuong;
    }

    public void setDoiTuong(String doiTuong) {
        this.doiTuong = doiTuong;
    }

    public int getDinhMuc() {
        return dinhMuc;
    }

    public void setDinhMuc(int dinhMuc) {
        this.dinhMuc = dinhMuc;
    }
    public int Thanhtien(){
        if(soLuong < dinhMuc){
            Thanhtien = soLuong*donGia;
        }
        else {
            Thanhtien = (int) (soLuong*donGia*dinhMuc + (dinhMuc - soLuong )*donGia*2.5);
        }
        return Thanhtien;
    }

    @Override
    public String toString() {
        return super.toString() +"  đối tương: "+this.doiTuong+"  Định mức: "+this.dinhMuc+"  Thành tiền: "+ this.Thanhtien();
    }
}
