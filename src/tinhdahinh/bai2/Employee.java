package tinhdahinh.bai2;

public class Employee extends Person{
        private double salaryIndex;
        private int salary;

        public Employee(String fullName, String address, double salaryIndex, int salary) {
            super(fullName, address);
            this.salaryIndex = salaryIndex;
            this.salary = salary;
        }
        public double getSalaryIndex() {
            return salaryIndex;
        }

        public void setSalaryIndex(double salaryIndex) {
            this.salaryIndex = salaryIndex;
        }

        public int getSalary() {
            return salary;
        }

        public void setSalary(int salary) {
            this.salary = salary;
        }

        public double getTotalSalary() {
            return (salaryIndex*salary);
        }

        @Override
        public String toString() {
            return "Employee{" +
                    super.toString() +
                    "salaryIndex=" + salaryIndex +
                    ", salary=" + salary +
                    ", TotalSalary=" + getTotalSalary() +
                    '}';
        }
}
