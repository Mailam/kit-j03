package tinhdahinh.bai2;

import java.util.Arrays;

public class Manager {
    private Person[] personArrays;

    public Manager(Person[] personArrays) {
        this.personArrays = personArrays;
    }

    public Person[] getPersonArrays() {
        return personArrays;
    }

    public void setPersonArrays(Person[] personArrays) {
        this.personArrays = personArrays;
    }

    public void addPerson(Person newPerson, int index) {
        this.personArrays[index] = newPerson;
    }

    public Person[] deletePerson(String name) {
        for(int i = 0; i < this.personArrays.length; i++) {
            Person person = this.personArrays[i];
            if (person.getFullName().contains(name)) {
                for(int j = i; j < this.personArrays.length-1; j++) {
                    this.personArrays[j] = this.personArrays[j+1];
                }
                break;
            }
        }

        Person[] newPersonArrays = new Person[this.personArrays.length-1];
        for (int i = 0; i < newPersonArrays.length; i++) {
            newPersonArrays[i] = this.personArrays[i];
        }
        return newPersonArrays;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "personArrays=" + Arrays.toString(this.personArrays) +
                '}';
    }
}
