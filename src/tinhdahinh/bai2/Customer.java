package tinhdahinh.bai2;

public class Customer extends Person{
    private String companyName;
    private int invoice;

    public Customer(String fullName, String address, String companyName, int invoice) {
        super(fullName, address);
        this.companyName = companyName;
        this.invoice = invoice;
    }

    @Override
    public String toString() {
        return "Customer{" +
                super.toString() +
                "name='" + companyName + '\'' +
                ", invoice=" + invoice +
                '}';
    }
}
