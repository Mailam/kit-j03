package tinhdahinh.bai2;

public class Student extends Person {
    private double pointLesson1;
    private double pointLesson2;

    public Student(String fullName, String address, double pointLesson1, double pointLesson2) {
        super(fullName, address);
        this.pointLesson1 = pointLesson1;
        this.pointLesson2 = pointLesson2;
    }

    public Student(String fullName, double pointLesson1, double pointLesson2) {
        super(fullName);
        this.pointLesson1 = pointLesson1;
        this.pointLesson2 = pointLesson2;
    }

    public double getAveragePoint() {
        double averagePoint = (pointLesson1 + pointLesson2) / 2;
        return averagePoint;
    }

    @Override
    public String toString() {
        return "Student{" +
                super.toString() +
                "pointLesson1=" + pointLesson1 +
                ", pointLesson2=" + pointLesson2 +
                ", averagePoint=" + getAveragePoint() +
                '}';
    }
}
