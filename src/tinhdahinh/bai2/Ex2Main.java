package tinhdahinh.bai2;

public class Ex2Main {
    public static void main(String[] args) {
        Student student = new Student("Hoang Tuan", "Nam Dinh", 6.5, 7.5);
        Employee employee = new Employee("Nguyen Duy", "Ha Noi", 1.2, 3000);
        Customer customer = new Customer("Hoan", "Ha Noi", "KMA", 45000);

        Person[] personArrays = new Person[3];
        Manager manager = new Manager(personArrays);

        // ADD Person
        System.out.println("ADD");
        manager.addPerson(student, 0);
        manager.addPerson(employee, 1);
        manager.addPerson(customer, 2);

        System.out.println(manager.toString());

        // DELETE
        System.out.println("DELETE");
        manager.setPersonArrays(manager.deletePerson("Hoan"));
        System.out.println(manager.toString());
    }
}
