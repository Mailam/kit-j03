package opp1.b4;

import java.util.Scanner;

public class connguoi {
    String Hoten;
    int Namsinh;
    String Quequan;
    String Gioitinh;
    public connguoi(String Hoten, String Quequan, String Gioitinh, int Namsinh){
        this.Hoten = Hoten;
        this.Namsinh =  Namsinh;
        this.Quequan =  Quequan;
        this.Gioitinh =  Gioitinh;
    }

    public String getHoten() {
        return Hoten;
    }

    public void setHoten(String hoten) {
        Hoten = hoten;
    }

    public int getNamsinh() {
        return Namsinh;
    }

    public void setNamsinh(int namsinh) {
        Namsinh = namsinh;
    }

    public String getQuequan() {
        return Quequan;
    }

    public void setQuequan(String quequan) {
        Quequan = quequan;
    }

    public String getGioitinh() {
        return Gioitinh;
    }

    public void setGioitinh(String gioitinh) {
        Gioitinh = gioitinh;
    }

    void info(){
        System.out.println("Ho ten: "+ this.Hoten);
        System.out.println("Que quan: "+ this.Quequan);
        System.out.println("Gioi tinh: "+ this.Gioitinh);
        System.out.println("Nam sinh: "+ this.Namsinh);
    }
}
