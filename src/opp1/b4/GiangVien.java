package opp1.b4;

public class GiangVien extends  connguoi {
    private String Khoa;
    private String Monhoc;
    private int Namkinnghiem;
    public GiangVien(String Hoten, String Quequan, String Gioitinh, int Namsinh){
        super(Hoten,Quequan,Gioitinh,Namsinh);
    }

    public String getKhoa() {
        return Khoa;
    }

    public void setKhoa(String khoa) {
        Khoa = khoa;
    }

    public String getMonhoc() {
        return Monhoc;
    }

    public void setMonhoc(String monhoc) {
        Monhoc = monhoc;
    }

    public int getNamkinnghiem() {
        return Namkinnghiem;
    }

    public void setNamkinnghiem(int namkinnghiem) {
        Namkinnghiem = namkinnghiem;
    }
    void info() {
        super.info();
        System.out.println("Mon hoc: "+ this.Monhoc);
        System.out.println("Khoa: "+ this.Khoa);
        System.out.println("Nam kinh nghiem: "+ this.Namkinnghiem);
    }
    void Gt(sinhVien sv, GiangVien gv){
        if(sv.getGioitinh().equalsIgnoreCase("Nu")){
            sv.info();
        }
        if(gv.getGioitinh().equalsIgnoreCase("Nu")){
           gv.info();
        }
    }
}
